from django.db import models
import uuid



class Car(models.Model):

    GENRES_TYPE_CHOICES = (
    ('NONE', 'GENEROS'),
    ('Religious', 'RELIGIOSO'),
    ('Tales', 'CONTOS'),
    ('Affairs', 'ROMANCE'),
    ('Comedy', 'COMÉDIA')
)


    book_id = models.UUIDField(
        primary_key=True,
        auto_created=True,
        default=str(uuid.uuid4())
    )
    
    book_year = models.IntegerField()
    description = models.TextField(max_length=1024)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    photo_car = models.ImageField(upload_to="photos/")

    def to_dict(self):
        return {
            "book_id": str(self.car_id),
            "book_year": self.car_year,
            "description": self.description,
            "price": self.price,
            "photo_car": self.photo_car
        }

    def __str__(self):
        return f"book: {self.book_id}"