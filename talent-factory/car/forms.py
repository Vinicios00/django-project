from django import forms

from car.models import Car 



class CarForms(forms.ModelForm):
    GENRES_TYPE_CHOICES = (
    ('NONE', 'GENEROS'),
    ('Religious', 'RELIGIOSO'),
    ('Tales', 'CONTOS'),
    ('Affairs', 'ROMANCE'),
    ('Comedy', 'COMÉDIA')
)

    book_id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                        'placeholder:text-app-blue-300 py-1 placeholder:text-sm hidden',
                'placeholder': 'INPUT THE BOOK',
                'readonly': True
            }
        )
    )

    book_year = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                        'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE BOOK YEAR'
            }
        )
    )

    price = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                        'placeholder:text-app-blue-300 py-1 placeholder:text-sm',
                'placeholder': 'INPUT THE BOOK PRICE'
            }
        )
    )

    description = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': 'bg-app-gray-400 rounded-none px-3 placeholder:font-semibold placeholder:font-roboto '
                        'placeholder:text-app-blue-300 py-2 placeholder:text-sm w-full h-36',
                'placeholder': 'DESCRIBE THE MAIN INFORMATION ABOUT THE BOOK'
            }
        )
    )


    photo_car = forms.ImageField()

    class Meta:
        model = Car
        fields = "__all__"

        def __init__(self, detail: bool =False, *args, **kwargs):
            super(CarForms, self).__init__(*args, **kwargs)

            for _, field in self.fields.items():
                

                if detail:
                    field.widget.attrs["disable"] = True

                    