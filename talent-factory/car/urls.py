from django.urls import path

from car.views import CarsListView, CarDetailView, CarsCreateView, CarsDeleteView

urlpatterns = [
    
    path("", CarsListView.as_view(), name="cars-list"),
    path("register/", CarsCreateView.as_view(), name="cars-create"),
    path("<str:book_id>/detail/", CarDetailView.as_view(), name="cars-detail"),
    path("<str:book_id>/", CarsDeleteView.as_view(), name="cars-delete")
]

